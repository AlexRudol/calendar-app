# Calendar App

Sample Calendar App.

## What's in

- **React 18**, **TypeScript 5**
- [**React Aria**](https://react-spectrum.adobe.com/react-aria/) for components inner structure, built-in behavior, adaptive interactions, a11y etc.
- [**Tailwind 3**](https://tailwindcss.com/) for styles - quick prototyping.

## How to

Just standard set:
- `nvm use` to set the local Node version;
- `yarn install` to get dependencies;
- `yarn start` for the local dev;
- `yarn build` to assemble.

## Ways to improve (TODO)

- **Atoms** for: _Date Picker_ and _Recurrence Pattern_ toggle.
- **React Native**:
  - add [_React Native Aria_](https://react-native-aria.geekyants.com/) to support _React Aria_ components in _React Native_;
  - add [_NativeWind_](https://react-native-aria.geekyants.com/) to support _Tailwind_ styles in _React Native_.
- **Unit tests** and e2e.

## Notes

- WIP: Recurring Events are displayed as a single Event, yet the logic to support it is in dev.
