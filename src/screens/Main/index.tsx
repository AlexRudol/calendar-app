import React, { Suspense, useContext } from "react";
import { DataStoreContext } from "../../store/DataStoreContext";
import { CalendarBoard } from "../../organisms/CalendarBoard";
import { EventForm } from "../../organisms/EventForm";

const Main: React.FC = () => {
    const { isLoaded } = useContext(DataStoreContext);

    return (
        <>
            <div className="flex justify-center flex-wrap w-full">
                <Suspense fallback={<div>LOADING...</div>}>
                    {/* @TODO: Add a Skeleton loader */}
                    <CalendarBoard isReady={isLoaded} />
                </Suspense>
                <EventForm />
            </div>
        </>
    );
};

export default Main;
