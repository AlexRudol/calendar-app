import React from "react";
import Main from "./screens/Main";
import "./App.css";
import { DataStoreContextProvider } from "./store/DataStoreContext";

const App: React.FC = () => (
    <DataStoreContextProvider>
        <Main />
    </DataStoreContextProvider>
);

export default App;
