export type EventId = number;

export interface CalendarEvent {
    id: EventId;
    title: string;
    description?: string | undefined;
    startDate: Date;
    endDate?: Date | undefined;
    recurring?: boolean;
    recurrencePattern?: RecurrencePattern | undefined;
}

export enum RecurrencePattern {
    WEEKLY = "weekly",
    FORTNIGHTLY = "fortnightly",
    MONTHLY = "monthly",
    ANNUALLY = "annually"
}
