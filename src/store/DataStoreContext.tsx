import React, { Context, createContext, PropsWithChildren, useEffect, useMemo, useState } from "react";
import { CalendarEvent, EventId } from "../types/CalendarEvent";

/**
 *
 * Data Store Context
 * ==============
 * Get, store and transform the global data: Calendar events, external events etc.
 *
 */

/*
 * Data sources
 */
const MOON_PHASE_DATA_SOURCE: string = "https://craigchamberlain.github.io/moon-data/api/moon-phase-data/";
const CURRENT_YEAR: number = new Date().getFullYear();

// Just a temp ID for now
export const randomId = (): number => {
    return Math.floor(Math.random() * 10 * 10 * 1000);
};

export const NEW_EVENT_TEMPLATE: CalendarEvent = {
    id: randomId(),
    title: "New Event",
    startDate: new Date()
};

/*
 * Context store
 * to share data across components
 */
type DataStoreContextType = {
    isLoaded: boolean;
    setIsLoaded: React.Dispatch<React.SetStateAction<boolean>>;
    calendarEvents: CalendarEvent[];
    setCalendarEvents: React.Dispatch<React.SetStateAction<CalendarEvent[]>>;
    recurringEvents: CalendarEvent[];
    setRecurringEvents: React.Dispatch<React.SetStateAction<CalendarEvent[]>>;
    selectedEventId: EventId | undefined;
    setSelectedEventId: React.Dispatch<React.SetStateAction<number | undefined>>;
};

/*
 * Context store initial values
 */
export const defaultDataStoreContextState: DataStoreContextType = {
    isLoaded: false,
    setIsLoaded: () => {},
    calendarEvents: [],
    setCalendarEvents: () => {},
    recurringEvents: [],
    setRecurringEvents: () => {},
    selectedEventId: undefined,
    setSelectedEventId: () => {}
};

export const DataStoreContext: Context<DataStoreContextType> =
    createContext<DataStoreContextType>(defaultDataStoreContextState);

export const DataStoreContextProvider = ({ children }: PropsWithChildren<{}>) => {
    const [isLoaded, setIsLoaded] = useState<boolean>(false);
    const [calendarEvents, setCalendarEvents] = useState<CalendarEvent[]>([]);
    const [recurringEvents, setRecurringEvents] = useState<CalendarEvent[]>([]);
    const [selectedEventId, setSelectedEventId] = useState<EventId | undefined>(undefined);

    /*
     * Load Data: Timeline Cases, Mobs and Cows
     */
    useEffect(() => {
        const getData = async (): Promise<boolean> => {
            try {
                /*
                 * Fetch the external events data save it in the Data Store Context.
                 */
                // Moon Phase Events
                const moonPhaseEventsResponse: Response = await fetch(MOON_PHASE_DATA_SOURCE + CURRENT_YEAR);
                const moonPhaseEventsData = await moonPhaseEventsResponse.json();
                // Ensure the data source returned the data
                if (moonPhaseEventsData) {
                    const transformedMoonPhaseEventsData: CalendarEvent[] = moonPhaseEventsData.map(
                        (event: { Date: Date; Phase: number }) => ({
                            id: randomId(),
                            title: `Moon phase: ${event.Phase}`,
                            description: `Moon phase for this day is ${event.Phase}`,
                            startDate: event.Date
                        })
                    );
                    setCalendarEvents(transformedMoonPhaseEventsData);
                    setIsLoaded(true);
                    return Promise.resolve(true);
                } else {
                    return Promise.reject(false);
                }
            } catch (error) {
                console.error(error);
                throw false;
            }
        };
        // As this is the read only case, we don't subscribe to data source changes - load it only once.
        if (!calendarEvents.length) {
            getData();
        }
    }, [calendarEvents]);

    const value = useMemo(
        () => ({
            isLoaded,
            setIsLoaded,
            calendarEvents,
            setCalendarEvents,
            recurringEvents,
            setRecurringEvents,
            selectedEventId,
            setSelectedEventId
        }),
        [isLoaded, calendarEvents, recurringEvents, selectedEventId]
    );

    return <DataStoreContext.Provider value={value}>{children}</DataStoreContext.Provider>;
};
