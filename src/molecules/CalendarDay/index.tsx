import * as React from "react";
import { CalendarCell } from "react-aria-components";
import { CalendarDate } from "@internationalized/date";
import cn from "classnames";
import { CalendarEvent } from "../../types/CalendarEvent";
import { useContext } from "react";
import { DataStoreContext } from "../../store/DataStoreContext";

/**
 *
 * Calendar Day
 * ==============
 * Represent a Day with the list of Events
 */
export type CalendarDayProps = {
    date: CalendarDate;
    events: CalendarEvent[];
    className?: string;
};

export const CalendarDay: React.FC<CalendarDayProps> = ({ date, events, className }) => {
    const { setSelectedEventId } = useContext(DataStoreContext);

    return (
        <CalendarCell
            className={cn("p-2 w-32 h-32 bg-neutral-100 hover:bg-neutral-200 transition-all", className)}
            date={date}
        >
            <div className="text-lg text-neutral-500">{date.day}</div>
            {events.map((calendarEvent: CalendarEvent) => (
                <div
                    key={calendarEvent.id}
                    className="text-xs bg-yellow-600 text-white py-1 px-2 mb-0.5 hover:bg-yellow-500 transition-all"
                    onClick={() => {
                        setSelectedEventId(calendarEvent.id);
                    }}
                >
                    {calendarEvent.title}
                </div>
            ))}
        </CalendarCell>
    );
};
