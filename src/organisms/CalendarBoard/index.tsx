import React, { useContext } from "react";
import { Button, Calendar, CalendarGrid, Heading } from "react-aria-components";
import { CalendarDate, getLocalTimeZone } from "@internationalized/date";
import { CalendarDay } from "../../molecules/CalendarDay";
import { DataStoreContext, NEW_EVENT_TEMPLATE } from "../../store/DataStoreContext";
import { CalendarEvent } from "../../types/CalendarEvent";

/**
 *
 * Calendar
 * ==============
 * Main Calendar component: list of saved Events
 */

export type CalendarBoardProps = {
    isReady: boolean;
    className?: string;
};

export const CalendarBoard: React.FC<CalendarBoardProps> = ({ className }) => {
    const { calendarEvents, setCalendarEvents, setSelectedEventId } = useContext(DataStoreContext);

    const getDailyEvents = (date: CalendarDate) => {
        const processDate: Date = date.toDate(getLocalTimeZone());
        const recurringEventsForThisDay = []; // TODO: Pick the events for the process Day based on its pattern.
        const calendarEventsForThisDay = calendarEvents.filter((processEvent: CalendarEvent) => {
            // Display the same Event between Start and End dates, if recurring
            if (processEvent.endDate) {
                return (
                    processDate.toDateString().valueOf() >= new Date(processEvent.startDate).toDateString().valueOf() &&
                    processDate.toDateString().valueOf() <= new Date(processEvent.endDate).toDateString().valueOf()
                );
            }
            // Display just the Event for the process Day
            else {
                return processDate.toDateString() === new Date(processEvent.startDate).toDateString();
            }
        });
        return calendarEventsForThisDay.concat(recurringEventsForThisDay);
    };

    const addEvent = () => {
        const newEvent: CalendarEvent = NEW_EVENT_TEMPLATE;
        calendarEvents.push(newEvent);
        setCalendarEvents([...calendarEvents]);
        setSelectedEventId(newEvent.id);
    };

    return (
        <div>
            <Button onPress={addEvent}>Add Event</Button>
            <Calendar aria-label="Sample Calendar" className={className}>
                <header className="flex gap-4 my-8">
                    <Button slot="previous">◀</Button>
                    <Heading className="font-semibold" />
                    <Button slot="next">▶</Button>
                </header>
                <CalendarGrid>{(date) => <CalendarDay date={date} events={getDailyEvents(date)} />}</CalendarGrid>
            </Calendar>
        </div>
    );
};
