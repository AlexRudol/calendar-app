import React, { useContext, useEffect } from "react";
import cn from "classnames";
import { DataStoreContext, NEW_EVENT_TEMPLATE, randomId } from "../../store/DataStoreContext";
import { parseAbsoluteToLocal } from "@internationalized/date";
import { CalendarEvent, RecurrencePattern } from "../../types/CalendarEvent";
import {
    Button,
    Checkbox,
    DateField,
    DateInput,
    DateSegment,
    Form,
    Input,
    Label,
    Radio,
    RadioGroup,
    TextField
} from "react-aria-components";

/**
 *
 * Event Side Panel Form
 * ==============
 * A side panel to View/Edit Calendar Event's properties
 */

export type EventFormProps = {
    className?: string;
};

export const EventForm: React.FC<EventFormProps> = ({ className }) => {
    const { calendarEvents, setCalendarEvents, selectedEventId, setSelectedEventId } = useContext(DataStoreContext);
    let selectedEvent: CalendarEvent = calendarEvents.find((processEvent) => processEvent.id === selectedEventId);
    const selectedEventIndex: number = calendarEvents.indexOf(selectedEvent);

    const [title, setTitle] = React.useState<string>("");
    const [description, setDescription] = React.useState<string>("");
    const [startDate, setStartDate] = React.useState<Date>(new Date());
    const [endDate, setEndDate] = React.useState<Date | undefined>(undefined);
    const [recurring, setRecurring] = React.useState<boolean>(false);
    const [recurrencePattern, setRecurrencePattern] = React.useState<string>(undefined);

    /*
     * Set Form values / Reset form
     */
    useEffect(() => {
        if (selectedEvent && selectedEvent.title) {
            setTitle(selectedEvent.title);
            setDescription(selectedEvent.description ? selectedEvent.description : undefined);
            setStartDate(selectedEvent.startDate);
            setEndDate(selectedEvent.endDate);
            setEndDate(selectedEvent.endDate ? selectedEvent.endDate : undefined);
            setRecurring(selectedEvent.recurring ? selectedEvent.recurring : false);
            setRecurrencePattern(selectedEvent.recurrencePattern ? selectedEvent.recurrencePattern : undefined);
        }
    }, [selectedEventId, selectedEvent]);

    /*
     * Add new Event
     */
    const addEvent = () => {
        const newEvent: CalendarEvent = NEW_EVENT_TEMPLATE;
        newEvent.id = randomId();
        calendarEvents.push(newEvent);
        setCalendarEvents([...calendarEvents]);
        setSelectedEventId(newEvent.id);
    };

    /*
     * Save Edited Event
     */
    const saveEvent = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        selectedEvent = {
            id: selectedEvent.id,
            title: title,
            description: description,
            startDate: startDate,
            endDate: endDate,
            recurring: recurring,
            recurrencePattern: recurrencePattern as RecurrencePattern
        };
        calendarEvents[selectedEventIndex] = selectedEvent;
        setCalendarEvents([...calendarEvents]);
        setSelectedEventId(undefined);
    };

    const inputStyles: string = "block py-2 px-4 mb-4 bg-white";

    return (
        <div className={cn("w-96 border-l-2 border-neutral-200 ml-8 mt-8 p-8 pt-0", className)}>
            <Button onPress={addEvent} className="mt-4 mb-4 py-2 px-4 bg-neutral-300">
                Add Event
            </Button>

            {selectedEventId ? (
                <Form onSubmit={saveEvent}>
                    {/* Title */}
                    <TextField name="title">
                        <Label>Title</Label>
                        <Input
                            value={title}
                            className={inputStyles}
                            onChange={(event) => setTitle(event.target.value)}
                        />
                    </TextField>

                    {/* Description */}
                    <TextField name="description">
                        <Label>Description</Label>
                        <Input
                            value={description}
                            className={inputStyles}
                            onChange={(event) => setDescription(event.target.value)}
                        />
                    </TextField>

                    {/* Start Date */}
                    <DateField
                        value={parseAbsoluteToLocal(new Date(startDate).toISOString())}
                        onChange={(pickedDate) => setStartDate(pickedDate.toDate())}
                    >
                        <Label>Start date</Label>
                        <DateInput className={inputStyles}>
                            {(segment) => <DateSegment segment={segment} className="inline-block mr-0.5" />}
                        </DateInput>
                    </DateField>

                    {/* End Date */}
                    <DateField
                        value={endDate ? parseAbsoluteToLocal(new Date(endDate).toISOString()) : null}
                        onChange={(pickedDate) => setEndDate(pickedDate.toDate())}
                    >
                        <Label>End date</Label>
                        <DateInput className={inputStyles}>
                            {(segment) => <DateSegment segment={segment} className="inline-block mr-0.5" />}
                        </DateInput>
                    </DateField>

                    {/* Recurring */}
                    <Checkbox className="block mb-4" isSelected={recurring} onChange={() => setRecurring(!recurring)}>
                        <input type="checkbox" className="w-4 h-4 mr-2" checked={recurring} onChange={() => {}} />
                        Recurring
                    </Checkbox>

                    {/* Recurrence Pattern */}
                    <RadioGroup value={recurrencePattern ? recurrencePattern : null} onChange={setRecurrencePattern}>
                        <Label>Recurrence Pattern</Label>
                        {Object.entries(RecurrencePattern).map(([key, value]) => (
                            <Radio
                                key={key}
                                value={value}
                                className={({ isSelected }) => `block py-0.5 px-2
                                    ${isSelected ? "bg-amber-200" : ""}
                                `}
                            >
                                {value}
                            </Radio>
                        ))}
                    </RadioGroup>

                    <Button type="submit" className="mt-4 py-2 px-4 bg-amber-500 text-white">
                        SAVE
                    </Button>
                </Form>
            ) : null}
        </div>
    );
};
